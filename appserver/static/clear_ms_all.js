var selection = [];
/* The code below is for the first multiselect input. For additional, copy and paste it below. Then change multi1 to multi2, multi3, etc. and change input1 to input2, input3, etc. */
var multi1 = splunkjs.mvc.Components.getInstance("input1");
  multi1.on("change", function(){
    // get current selection
    selection = multi1.val();

    // check if there is more than one entry and one of them is "*"
    if (selection.length > 1 && ~(selection.indexOf("*"))) {
      if (selection.indexOf("*") == 0) {
        // "*" was first, remove it and leave rest
        selection.splice(selection.indexOf("*"), 1);
        multi1.val(selection);
        multi1.render();
      } else {
        // if "*" was added later, remove rest and leave it
        multi1.val("*");
        multi1.render();
      }
    }
  });
