"""Internal processing file."""

import logging
import json
import random

class ProgramLogger():
    """For controlling logging."""
    def __init__(self, filename=None, level="INFO"):

        self.event_id = random.randint(1, 9999999) #nosec
        self.record_id = None
        self.record_type = None
        self.message = {}


        if level.lower() == "debug":
            self.level = 0
        elif level.lower() == "info":
            self.level = 1
        elif level.lower() == "warn":
            self.level = 2
        elif level.lower() == "error":
            self.level = 3
        elif level.lower() == "crit":
            self.level = 4

        if filename is not None:
            self.set_logging(filename)

    def set_logging(self, filename):
        """Set logging standards."""
        if self.level >= 0:
            logging.basicConfig(
                filename=filename + '.log',
                level=logging.DEBUG,
                format='%(asctime)s - %(levelname)s - %(message)s')
        elif self.level >= 1:
            logging.basicConfig(
                filename=filename + '.log',
                level=logging.INFO,
                format='%(asctime)s - %(levelname)s - %(message)s')

    def trace_ids(self, rec_type, rec_id):
        """Addign Record Id."""
        self.record_id = rec_id
        self.record_type = rec_type

    def log_error_msg(self, message=None):
        """Log error message."""
        if self.level > 3:
            return
        self.build_msg()
        self.message['message'] = message
        logging.info(json.dumps(self.message))

    def log_info_msg(self, message=None):
        """Log info message."""
        if self.level > 1:
            return
        self.build_msg()
        self.message['message'] = message
        logging.info(json.dumps(self.message))

    def log_debug_msg(
            self,
            hcref=None,
            attribute=None,
            method=None,
            loop=None,
            target_attribute=None,
            target_method=None,
            message=None):
        """Log debug message."""

        if self.level > 0:
            return

        if target_method is not None and target_attribute is None:
            target_attribute = attribute

        if hcref is None:
            hcref = "!!!NOT_CONFIGURED!!!"

        self.build_msg()
        self.message['hcref'] = hcref
        self.optional_field("attribute", attribute)
        self.optional_field("method", method)
        self.optional_field("loop", loop)
        self.optional_field("target_attribute", target_attribute)
        self.optional_field("target_method", target_method)
        self.optional_field("rec_id", self.record_id)
        self.optional_field("rec_type", self.record_type)
        self.optional_field("message", message)
        logging.debug(json.dumps(self.message))

    def build_msg(self):
        """Central build for logging message."""
        self.message['event_id'] = self.event_id

    def optional_field(self, name, value):
        """Log optional fields in message."""
        if value is not None:
            self.message[name] = value
