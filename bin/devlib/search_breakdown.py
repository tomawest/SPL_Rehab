'''
File: search_breakdown.py
Copyright  Thomas West (tom.west1987@gmail.com)
This file is supplied in association with SPL Rehab and MUST NOT be
used outside the app without prior written permission
'''

import re
import random
from devlib.internal import ProgramLogger

def transform_search(search, action):
    """Search Transforms."""
    if action == "prefix_search":
        if (re.search("^\s*\|", search) is None and
                re.search("^\s*search", search) is None):
            return "search " + search
        return search
    return ""

def append_str(start, value, end=""):
    """Generic append to string."""
    return start + value + end

def parse_str(value, mode=None, mod=" "):
    """Generic string parse."""
    if mode is None:
        return value

    if mode == "NewLine":
        return value.replace("\\n", "").replace("\\r", "")

    if mode == "FullNewLine":
        return value.replace("\\n", "").replace("\\r", "")

    if mode == "Trim":
        return value.lstrip(mod).rstrip(mod)

    return value

def check_subs(i_subs, statement):
    """Check for sub searches."""
    i_subs += tot_checks(statement, "\[")
    i_subs -= tot_checks(statement, "\]")
    return i_subs

def tot_checks(statement, check):
    """Confirm Total Checks."""
    checks = re.split(check, statement)
    if checks is not None:
        return len(checks)
    return 0

class SearchBreakdown():
    """Main Breakdown for Search."""

    def __init__(
            self,
            search,
            exclusion_list="",
            prefix_search=False):

        # Set Defaults
        self.searches = []
        self.search_list = []
        self.generating = []
        self.state_count = []
        self.search_lines = []
        self.full_searches = []
        self.search_to_line = []
        self.combined = []
        self.command = []
        self.test = []
        self.debug = []
        self.event_id = str(random.randint(0, 1000000)) #nosec
        self.lines = ""
        self.i_search_loop = 0
        self.i_subs = 0
        self.i_quotes = 0
        self.in_quotes = False
        self.backquoted = False
        self.search_line = ""
        self.i_lines = 0
        self.remaining = ""
        self.logmsg = ProgramLogger()

        # Set Params
        self.exclusion_list = exclusion_list
        self.prefix_search = prefix_search

        if isinstance(search, list) == 0:
            self.searches.append(search)
        else:
            self.searches = search

        self.logmsg.log_info_msg(message="Starting Breakdown")

        self.logmsg.log_debug_msg(
            hcref="INST",
            attribute="searchBreakdown",
            method="__init__",
            target_method="loop_search",
            message="Initiating Loop")

        self.loop_search()

    def loop_search(self):
        """Loop through search breakers."""
        i_loop = 0
        for search in self.searches:
            search = parse_str(search, "FullNewLine")
            i_loop += 1
            self.logmsg.log_debug_msg(
                hcref="LSRINIT",
                attribute="searchBreakdown",
                method="loop_search",
                loop=i_loop,
                target_method="process_search",
                message="Starting Loop")
            self.process_search(search)

    def loop_statement(self, search):
        """Loop through search statement."""

        searchsplit = search.split("|")
        self.lines = ""
        self.search_line = ""
        self.i_search_loop = 0
        self.i_subs = 0
        self.i_quotes = 0
        self.i_lines = 0
        self.in_quotes = False
        self.backquoted = False
        i_loop = 0

        for statement in searchsplit:
            i_loop += 1
            self.logmsg.log_debug_msg(
                hcref="LSTINIT",
                attribute="searchBreakdown",
                method="loop_statement",
                loop=i_loop,
                target_method="process_statement",
                message="Starting Loop")

            self.process_statement(statement)

    def loop_quotes(self, statement, quote_search):
        """Loop through search quotes."""


        i_loop = 0

        while quote_search is not None:
            i_loop += 1
            self.logmsg.log_debug_msg(
                hcref="LSTINIT",
                attribute="searchBreakdown",
                method="loop_quotes",
                loop=i_loop,
                target_method="process_quotes",
                message="Starting Loop")
            quote_search = self.process_quotes(
                statement,
                quote_search)

    def process_search(self, search):
        """Internal process inside search loop."""

        if self.check_exclusions(search) == "Next":
            return

        if self.prefix_search is True:
            search = transform_search(search, "prefix_search")

        self.loop_statement(search)
        self.state_count.append(self.i_lines)
        self.full_searches.append(self.lines)

    def process_statement(self, statement):
        """Internal process inside statement loop."""

        has_quotes = re.search("[\"\']", statement)
        self.remaining = ""
        self.i_search_loop = self.i_search_loop + 1

        if has_quotes is None:
            if self.in_quotes is False:
                self.i_subs = check_subs(self.i_subs, statement)
            statement = parse_str(
                statement,
                "NewLine")
            self.lines = append_str(
                self.lines,
                statement,
                "|")
            self.search_line = append_str(
                self.search_line,
                statement,
                "|")
        else:
            self.loop_quotes(statement, has_quotes)

        if self.in_quotes is False and self.i_subs == 0 and self.lines != "|":
            self.i_lines += 1
            self.search_line = parse_str(
                self.search_line,
                "Trim",
                mod=" ")
            self.search_line = parse_str(
                self.search_line,
                "Trim",
                mod="|")
            if self.i_lines == 1:
                self.generating.append(self.search_line)

            command_check = re.search(
                "^\s*((?P<Command>\w+)|\`(?P<isMacro>[^\`]+))",
                self.search_line)
            if command_check is None:
                command = "!!!UNKNOWN!!!"
            else:
                if command_check.group("Command") is not None:
                    command = command_check.group("Command")
                elif command_check.group("isMacro") is not None:
                    command = "Macro:" + command_check.group("isMacro")
                else:
                    command = "!!!UNKNOWN!!!"

            self.search_lines.append(self.search_line)
            self.search_to_line.append(self.lines)
            self.command.append(command)
            self.combined.append(
                command +
                "``RehabSEP``" +
                self.lines +
                "``RehabSEP``" +
                self.search_line)

            self.test.append(self.search_line)
            self.debug.append(self.in_quotes)
            self.search_line = ""

    def process_quotes(self, statement, quote_search):
        """Internal process inside quote loop."""

        quoter = quote_search.group()

        if not self.remaining == "":
            self.in_quotes = False
            statement = self.remaining

        self.remaining = ""

        if self.in_quotes:
            split_state = re.search(
                "^(?P<Quoted>(\\\\" +
                quoter + "|[^\\" +
                quoter + "])*\\" +
                quoter + "?)(?P<Remaining>.*)",
                statement)
            if split_state is None:
                return ""
        else:
            split_state = re.search(
                "^(?P<Start>[^\\" +
                quoter + "]+\\" +
                quoter + ")(?P<Quoted>(\\\\" +
                quoter + "|[^\\" +
                quoter + "])*\\" +
                quoter + "?)(?P<Remaining>.*)",
                statement)

            if split_state is None:
                return ""

            self.i_subs = check_subs(self.i_subs, split_state.group("Start"))
            self.lines = append_str(
                self.lines,
                split_state.group("Start"))
            self.search_line = append_str(
                self.search_line,
                split_state.group("Start"))

        quoted_str = split_state.group("Quoted")
        check_quotes = re.search(
            "[^\\\]\\" +
            quoter + "$",
            quoted_str)

        if check_quotes is not None:
            self.in_quotes = False
        else:
            self.in_quotes = True

        self.lines = append_str(self.lines, quoted_str)
        self.search_line = append_str(self.search_line, quoted_str)

        self.remaining = split_state.group("Remaining")

        self.logmsg.log_debug_msg(
            hcref="PQMIDP",
            attribute="searchBreakdown",
            method="process_quotes",
            message="self.in_quotes=" + str(self.in_quotes) +
            "&self.lines=" + self.lines +
            "&self.search_line=" + self.search_line +
            "&self.remaining=" + self.remaining)

        quote_search = re.search("[\"\']", self.remaining)
        if quote_search is None:

            self.i_subs = check_subs(self.i_subs, self.remaining)
            self.lines = append_str(
                self.lines,
                self.remaining,
                "|")
            self.search_line = append_str(
                self.search_line,
                self.remaining,
                "|")

        return quote_search

    def check_exclusions(self, search):
        """Check for Exclusions."""
        for exclusion in self.exclusion_list:
            if re.search(exclusion, search) is not None:
                return "Next"
        return "OK"



    def test_search(self):
        """Module for testing search."""
        return self.searches
